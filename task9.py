
inputs = [int(value) for value in input().split()]
groupA = []
groupB = {}

for i in range(0, inputs[0]):
    groupA.append(input())    
    groupB[groupA[i]] = groupB.setdefault(groupA[i], 0) + 1
print(len(groupB))
for item in groupB:
    print(groupB[item], end=" ")
print()
