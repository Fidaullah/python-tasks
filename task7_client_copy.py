import time
import json
import asyncio
import websockets
import argparse
import logging


uri = "ws://localhost:5000"
data = []
data_ret = []

def get_data(file_name):
    logger.info(f"fetching data from : {file_name}")
    with open(file_name, "r") as file1:
        return json.load(file1)


async def client(uri, data):
    logger.info(f"establishing connection with : {uri}")
    async with websockets.connect(uri) as websocket:
        logger.info("connections established")
        count = 10 # len(data)
        for i in range(count):
            message = data[i]
            logger.debug(f"sending : {message}")
            message['tx_time'] = time.time_ns()
            await websocket.send(json.dumps(message))
            data_ret.append(await websocket.recv())
            logger.debug(f"recevied message {data_ret[-1]}")
    logger.info("connection ended")


# argparser-----------------------
parser = argparse.ArgumentParser(description="websocket client side script")
parser.add_argument(
    "Messages", help="path to file than contains list of messages to send on websocket")
logs = parser.add_mutually_exclusive_group()
logs.add_argument("-q", "--quiet", action="store_false", default=True, help="give this flag to stop logs")
logs.add_argument("-f", "--file", type=str, help="use to specify where to save logs")
args = parser.parse_args()


logger = logging.getLogger("client_log")
logger.setLevel(level=logging.DEBUG)

if args.file is not None:
    fileh = logging.FileHandler(filename=args.f)
    fileh.setLevel(logging.DEBUG)
    logger.addHandler(fileh)
elif args.quiet:
    streamh = logging.StreamHandler()
    streamh.setLevel(logging.INFO)
    logger.addHandler(streamh)

# print(args)
data = get_data(args.Messages)
asyncio.get_event_loop().run_until_complete(client(uri, data))

