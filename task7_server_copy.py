import json
import asyncio
import logging
import websockets
import time
import argparse
from pprint import pprint


uri = 'localhost'
port = 5000


async def server(websocket, path):
    logger.info("Connection established")
    async for message in websocket:
        logger.debug(f"recived following message: {message}")
        ts = time.time_ns()
        message = json.loads(message)
        message['rx_time'] = ts
        msg_latency = (ts - message["tx_time"])
        message['msg_latency'] = msg_latency
        logger.debug(f"Reply with : {message}")
        await websocket.send(json.dumps(message))
        with open("msg_latency.txt", "a") as file:
            file.write(str(msg_latency) + "\n")
    logger.info("Connection Ended")


async def main():
    async with websockets.serve(server, uri, port):
        await asyncio.Future()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="websocket server side script")
    logs = parser.add_mutually_exclusive_group()
    logs.add_argument("-q", action="store_false", default=True,
                      help="give this flag to stop logs")
    logs.add_argument("-f", type=str, help="use to specify where to save logs")
    args = parser.parse_args()

    # websockets logs

    logger = logging.getLogger("server_log")
    logger.setLevel(level=logging.DEBUG)

    if args.f is not None:
        fileh = logging.FileHandler(filename=args.f)
        fileh.setLevel(logging.DEBUG)
        logger.addHandler(fileh)
    elif args.q:
        streamh = logging.StreamHandler()
        streamh.setLevel(logging.INFO)
        logger.addHandler(streamh)

    asyncio.run(main())
