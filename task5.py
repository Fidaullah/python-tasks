from matplotlib import pyplot

def square(values: list):
    return [x ** 2 for x in values]
     
def cube(values: list):
    return [x ** 3 for x in values]

X = [1, 2, 3 , 4, 5, 6, 7, 8, 9, 10]

pyplot.plot(X,square(X))
pyplot.xlabel("Natural numbers")
pyplot.ylabel("Returned values from functions")
pyplot.plot(X,cube(X))
pyplot.legend(['square','cube'])
pyplot.show()