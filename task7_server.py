import json
import asyncio
import websockets
import time
from pprint import pprint


uri = 'localhost'
port = 5000
msg_latency = []


async def server(websocket, path):
    async for message in websocket:
        ts = time.time_ns()
        message = json.loads(message)
        message['rx_time'] = ts
        msg_latency.append(ts - message["tx_time"])
        message['msg_latency'] = msg_latency[-1]
        pprint(message)
        await websocket.send(json.dumps(message))


async def save_data():
    count = 0
    async with open("msg_latency.txt", "w") as file:
        if count < len(msg_latency):
            await file.write(msg_latency[count])
            count += 1

async def main():
    event_loop = asyncio.get_event_loop()
    t1 = event_loop.create_task(save_data())
    t2 = event_loop.run_until_complete(websockets.serve(server, uri, port))
    event_loop.run_forever()
    return 0

if __name__ == "__main__":
    ret = asyncio.run(main())
    print(ret)

