import time
import json
import asyncio
import websockets
import argparse
import logging

logging.basicConfig(level=logging.INFO)
uri = "ws://localhost:5000"
data = []
data_ret = []


def get_data(file_name):
    logging.info(f"fetching data from : {file_name}")
    with open(file_name, "r") as file1:
        return json.load(file1)


async def client(uri, message):
    async with websockets.connect(uri) as websocket:
        message['tx_time'] = time.time_ns()
        await websocket.send(json.dumps(message))
        data_ret.append(await websocket.recv())


parser = argparse.ArgumentParser(description="websocket client side script")
parser.add_argument(
    "Messages", help="path to file than contains list of messages to send on websocket")
args = parser.parse_args()
data = get_data(args.Messages)

count = 10 # len(data)
for message in range(count):
    asyncio.get_event_loop().run_until_complete(client(uri, data[message]))
