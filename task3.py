import os

os.chdir(f"/home/{os.getenv('USER')}/")
if "Details" not in os.listdir():
    os.mkdir("Details")
os.chdir(f"/home/{os.getenv('USER')}/Details/")
if "Summary.txt" in os.listdir():
    os.remove("Summary.txt")

# os.path.exists()
# mem
a = os.popen("lscpu")
with open("Summary.txt", 'x') as summary:
    summary.write(a.read())