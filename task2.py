def distance(x: int, y: int) -> int:
    float_value = (x ** 2 + y ** 2) ** 0.5
    return round(float_value)



def distance_from_directions(directions: tuple):
    directions_lookup = {
        'UP': 'y+',
        'DOWN': 'y-',
        'LEFT': 'x-',
        'RIGHT': 'x+'
    }
    values = {
        'x+': 0,
        'x-': 0,
        'y+': 0,
        'y-': 0
    }

    for item in directions:
        direction, value = item.split()
        # print(direction, value)
        values[directions_lookup[direction]] += int(value)
   
    return distance(values['x+']-values['x-'], values['y+']-values['y-'])


# ________main__________
direction_list = ('UP 1', 'DOWN 13', 'RIGHT 5')

# add argparser 
import argparse
parser = argparse.ArgumentParser(description="This takes list of distances and directions and returns displacement.")
parser.add_argument('directions_list', nargs='+', help="please input direction and distance in following \
    'UP 3' \
    'RIGHT 4'")

args = parser.parse_args()
print(args)
direction_list = args.directions_list

print(distance_from_directions(direction_list))
