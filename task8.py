import datetime
import statistics
import sys

file_name = "msg_latency2.txt" if len(sys.argv) == 1 else sys.argv[1]


data = []
with open(file_name, 'r') as file:
    data = [float(value) / 1000 for value in file.readlines()]


mean = statistics.mean(data)
median = statistics.median(data)
stddevi = statistics.stdev(data)
min_ = min(data)
max_ = max(data)
count = len(data)
data.sort()


print(f"""--------------------------
Date-Time: {datetime.datetime.now()} 
Stats for Websocket Latency
--------------------------
Mean: {mean}us
Median: {median}us
Min: {min_}us
Max: {max_}us
Standard Deviation: {stddevi}us
90th percentile: {data[int(count * 0.9)]}us
99th percentile: {data[int(count * 0.99)]}us
99.9th percentile: {data[int(count * 0.999)]}us
99.99th percentile: {data[int(count * 0.9999)]}us
99.999th percentile: {data[int(count * 0.99999)]}us
Total values: {count}""")