import logging
import pprint
import time
import asyncio

logging.basicConfig(level=logging.INFO)

async def find_divisibles(_range: int, divisor: int):
    logging.info(f"find_divisible called with range {_range} and divisor {divisor}.")
    ts1 = time.time()
    ret_list = []
    for i in range(1,_range):
        if i % divisor == 0 :
            ret_list.append(i)
            time.sleep(0.001)           # task delay
            await asyncio.sleep(0.001)  # io task delay
    ts2 = time.time()
    logging.info(f"find_divisible ended with range {_range} and divisor {divisor}.\n \
        it took {ts2 - ts1} seconds")
    return ret_list

async def main():
    listA, listB, listC = await asyncio.gather( 
        find_divisibles(50800000, 34113),
        find_divisibles(100052, 3210),
        find_divisibles(500, 3)
        )
    pprint.pprint("this is find_divisible(100052, 3210) function's list")
    # pprint.pprint(listB)
    pprint.pprint("this is find_divisible(500, 3) function's list")
    # pprint.pprint(listC)

async def main2():
    loop = asyncio.get_event_loop()
    t1 = loop.create_task(find_divisibles(50800000, 34113))
    t2 = loop.create_task(find_divisibles(100052, 3210))
    t3 = loop.create_task(find_divisibles(500, 3))
    listA = await t1
    listB = await t2
    listC = await t3
   
    pprint.pprint("this is find_divisible(100052, 3210) function's list")
    # pprint.pprint(listB)
    pprint.pprint("this is find_divisible(500, 3) function's list")
    # pprint.pprint(listC)


if __name__ == "__main__":
    t1 = time.perf_counter()
    asyncio.run(main2())
    t2 = time.perf_counter()
    logging.info(f"total time to execute main = {t2 - t1}")
