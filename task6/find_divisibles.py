import logging
import pprint
import time

logging.basicConfig(level=logging.INFO)


def find_divisibles(_range: int, divisor: int):
    logging.info(f"find_divisible called with range {_range} and divisor {divisor}.")
    ts1 = time.time()
    ret_list = []
    for i in range(1,_range):
        if i % divisor == 0 :
            ret_list.append(i)
            time.sleep(0.001)   # task delay
            time.sleep(0.001)   # io task delay
    ts2 = time.time()
    logging.info(f"find_divisible ended with range {_range} and divisor {divisor}.\n \
        it took {ts2 - ts1} seconds")
    return ret_list


def main():
    listA = find_divisibles(50800000, 34113)
    listB = find_divisibles(100052, 3210)
    listC = find_divisibles(500, 3)

    pprint.pprint("this is find_divisible(100052, 3210) function's list")
    # pprint.pprint(listB)
    pprint.pprint("this is find_divisible(500, 3) function's list")
    # pprint.pprint(listC)


if __name__ == "__main__":
    t1 = time.perf_counter()
    main()
    t2 = time.perf_counter()
    logging.info(f"total time to execute main = {t2 - t1}")
