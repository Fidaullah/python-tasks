import json
import argparse
import random
# commenting 
# dont use  i as variables
# 

def monte_carlo_pi(iterations: int) -> float :
    circle = 0
    square = 0
    for i in range(iterations):
        x = random.random()
        y = random.random()
        d = x ** 2 + y ** 2 
        if d < 1:
            circle += 1
        square += 1
    return 4 * circle / square

def get_iterations(file_name: str) -> int :
    with open(file_name, 'r') as file1:
        data = json.load(file1)
        return data['iterations']

parser = argparse.ArgumentParser(description="estimates the value of pi using monte carlo's algorithim")
parser.add_argument("-i", metavar="iterations", type=int, help="number of iterations to do\
     for estimating the value of pi")
parser.add_argument("-j", metavar="json_file", type=str, help="to specify the file to get value of iterations from")
args = parser.parse_args()
print (args)
print(args.i)
print(args.j)

if args.i is not None:
    print(monte_carlo_pi(args.i))
elif args.j is not None:
    print(monte_carlo_pi(get_iterations(args.j)))
else:
    print(monte_carlo_pi(50))
